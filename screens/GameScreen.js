import React, { useState, useRef, useEffect } from "react";
import { Alert, View, Text, Button, StyleSheet } from "react-native";

import Card from "../components/Card";
import NumberContainer from "../components/NumberContainer";

const RandomNumber = (min, max, exclude) => {
  min = Math.ceil(min);
  let random = Math.floor(Math.random() * (max - min) + min);
  if (random === exclude) return RandomNumber(min, max, exlude);
  return random;
};

const GameScreen = ({ userNumber, gameOverHandler }) => {
  const [computerNumber, setComputerNumber] = useState(
    RandomNumber(0, 100, userNumber)
  );
  const [rounds, setRounds] = useState(0);
  const currentLow = useRef(1);
  const currentHigh = useRef(100);

  useEffect(() => {
    if (computerNumber === userNumber) {
      gameOverHandler(rounds);
    }
  }, [computerNumber, userNumber, gameOverHandler]);

  const guessHandler = direction => {
    if (
      (direction === "lower" && computerNumber < userNumber) ||
      (direction === "higher" && computerNumber > userNumber)
    ) {
      Alert.alert(
        "Nice Try",
        "We both know you are wrong, lets do that again...",
        [{ text: "Try Again", style: "cancel" }]
      );
      return;
    }
    switch (direction) {
      case "lower":
        currentHigh.current = computerNumber;
        break;
      case "higher":
        currentLow.current = computerNumber;
        break;
      default:
        break;
    }
    setComputerNumber(
      RandomNumber(currentLow.current, currentHigh.current, computerNumber)
    );
    setRounds(Rounds => Rounds + 1);
  };
  return (
    <View style={styles.screen}>
      <Text style={styles.title}>Welcome to the game!</Text>
      <Card style={styles.card}>
        <Text>Computer Guess:</Text>
        <NumberContainer>{computerNumber}</NumberContainer>
      </Card>
      <Card style={styles.buttonCard}>
        <View style={styles.btnGroup}>
          <View style={styles.btn}>
            <Button title="Lower" onPress={() => guessHandler("lower")} />
          </View>
          <View style={styles.btn}>
            <Button title="Higher" onPress={() => guessHandler("higher")} />
          </View>
        </View>
      </Card>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    marginTop: 10,
    flex: 1,
    alignItems: "center"
  },
  title: {
    fontFamily: 'raleway-bold'
  },
  card: {
    width: 250,
    maxWidth: "70%",
    alignItems: "center",
    padding: 20
  },
  buttonCard: {
    width: 300,
    maxWidth: "80%",
    alignItems: "center",
    padding: 20
  },
  btnGroup: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%"
  },
  btn: {
    width: 90
  },
  text: {
    padding: 10,
    fontFamily: 'raleway'
  }
});

export default GameScreen;
