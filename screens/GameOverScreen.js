import React from "react";
import { View, Text, Button, StyleSheet, Image } from "react-native";

import Card from "../components/Card";
import NumberContainer from "../components/NumberContainer";

const GameOverScreen = ({ userNumber, rounds, newGameHandler }) => {
  return (
    <View style={styles.screen}>
      <Text style={styles.title}>Game Over!</Text>
      <View style={styles.imageContainer}><Image source={require('../assets/images/fail.jpg')} resizeMode="contain" style={styles.image} /></View>
      <Card style={styles.cardMain}>
        <Text style={styles.text}>The computer guessed your number</Text>
        <Text style={styles.text}>Your Number:</Text>
        <NumberContainer>{userNumber}</NumberContainer>
      </Card>
      <Card style={styles.card}>
        <Text style={styles.text}>Computer took:</Text>
        <NumberContainer>{rounds}</NumberContainer>
        <Text style={styles.text}>Guesses</Text>
      </Card>
      <Button title="Start New Game" onPress={newGameHandler} style={styles.btn}/>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    marginTop: 10,
    fontSize: 20,
    padding: 20,
    color: 'red',
    fontFamily: 'raleway-bold'
  },
  card: {
    alignItems: "center",
    width: 200, 
    maxWidth: '70%'
  },
  cardMain: {
    alignItems: "center",
    width: 340, 
    maxWidth: '90%'
  },
  text: {
    padding: 10,
    fontFamily: 'raleway'
  },
  btn: {
    width: 80
  },
  imageContainer: {
    width: '80%',
    height: 100,
  },
  image: {
    width: '100%',
    height: '100%'
  }
});

export default GameOverScreen;
