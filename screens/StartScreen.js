import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Button,
  Keyboard,
  TouchableWithoutFeedback,
  Alert
} from "react-native";
import Card from "../components/Card";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";

const startScreen = props => {
  const [inputValue, setInputValue] = useState("");
  const [startNumber, setStartNumber] = useState();
  const [ready, setReady] = useState(false);

  const onInputNumberHandler = text => {
    setInputValue(text.replace(/[^0-9]/g, ""));
  };

  const onResestHandler = text => {
    setInputValue("");
    setReady(false);
  };

  const onConfirmHandler = () => {
    Keyboard.dismiss();
    let value = parseInt(inputValue);
    if (isNaN(value) || value <= 0) {
      Alert.alert(
        "Invalid Entry",
        "Sorry, but what you entered was not a valid number.",
        [{ text: "Try Again", style: "default" }]
      );
      setInputValue("");
      return;
    } else {
      setReady(true);
      setInputValue("");
      setStartNumber(parseInt(inputValue));
    }
  };

  let content;
  if (ready) {
    content = (
      <Card style={styles.startCard}>
        <Text>You selected:</Text>
        <NumberContainer>{startNumber}</NumberContainer>
        <Button title="Start Game" onPress={() => props.onPress(startNumber)} />
      </Card>
    );
  }

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.screen}>
        <Text style={styles.title}>Start a new game!</Text>
        <Card style={styles.card}>
          <Text>Select a Number</Text>
          <Input
            style={styles.input}
            keyboardType="number-pad"
            maxLength={2}
            onChangeText={onInputNumberHandler}
            value={inputValue}
          />
          <View style={styles.btnGroup}>
            <Button title="Reset" onPress={onResestHandler} />
            <Button title="Start" onPress={onConfirmHandler} />
          </View>
        </Card>
        {content}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default startScreen;

const styles = StyleSheet.create({
  screen: {
    marginTop: 10,
    flex: 1,
    alignItems: "center"
  },
  title: {
    color: "#3CA8FF",
    fontFamily: 'raleway-bold',
    paddingVertical: 10
  },
  card: {
    padding: 20,
    alignItems: "center",
    width: 320,
    maxWidth: "80%"
  },
  btnGroup: {
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
    padding: 10
  },
  input: {
    marginVertical: 5,
    width: 60,
    textAlign: "center"
  },
  startCard: {
    width: 200,
    alignItems: "center",
    padding: 20
  },
  text: {
    padding: 10,
    fontFamily: 'raleway'
  }
});
