import React from "react";
import { View, Text, StyleSheet } from "react-native";

const card = ({ children, style }) => {
  return <View style={{ ...styles.card, ...style }}>{children}</View>;
};

const styles = StyleSheet.create({
    card: {
        shadowColor: 'black',
        shadowOffset: {width: 0, height: 4},
        shadowRadius: 6,
        shadowOpacity: 0.25,
        backgroundColor: '#fff',
        elevation: 7,
        borderRadius: 10,
        padding: 10,
        marginVertical: 15
    }
})

export default card;
