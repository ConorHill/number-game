import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

const header = ({title}) => {
    return (
        <View style={styles.header}>
            <Text style={styles.headerTitle}>{title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 75,
        paddingTop: 50,
        backgroundColor: '#3CA8FF',
        alignItems: 'center'
    },
    headerTitle: {
        fontSize: 15,
        fontWeight: '800'
    }

})

export default header
