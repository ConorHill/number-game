import React from 'react'
import { TextInput, StyleSheet } from 'react-native'

const input = props => {
    return (
        <TextInput {...props} style={{...styles.input, ...props.style}} />
    )
}

const styles = StyleSheet.create({
    input: {
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        borderStyle: 'solid'
    }
})
export default input
