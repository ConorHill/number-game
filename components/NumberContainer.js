import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Colours from '../constants/colours';

const NumberContainer = ({children}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{children}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    borderColor: Colours.secondary,
    borderWidth: 3,
    borderRadius: 10,
    margin: 10
  },
  text: {
    padding: 10,
    fontSize: 16
  }
})


export default NumberContainer
