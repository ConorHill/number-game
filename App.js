import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import * as Font from "expo-font";
import { AppLoading } from "expo";

import Header from "./components/Header";
import StartScreen from "./screens/StartScreen";
import GameScreen from "./screens/GameScreen";
import GameOverScreen from "./screens/GameOverScreen";

const fetchFonts = () => {
  return Font.loadAsync({
    'raleway': require("./assets/fonts/Raleway-Regular.ttf"),
    'raleway-bold': require("./assets/fonts/Raleway-Bold.ttf")
  });
};

export default function App() {
  const [loaded, setLoaded] = useState(false);
  const [userNumber, setUserNumber] = useState();
  const [gameRounds, setGameRounds] = useState(0);

  if (!loaded){
    return (
      <AppLoading startAsync={fetchFonts} onFinish={() => setLoaded(true)} />
    );
  // AppLoading -> startAsync must return a promise
    }

  const startGameHandler = number => {
    setUserNumber(number);
  };

  const gameOverHandler = rounds => {
    setGameRounds(rounds);
  };

  const newGameHandler = () => {
    setUserNumber();
    setGameRounds(0);
  };

  let content = <StartScreen onPress={startGameHandler} />;

  if (userNumber && gameRounds <= 0)
    content = (
      <GameScreen userNumber={userNumber} gameOverHandler={gameOverHandler} />
    );

  if (userNumber && gameRounds > 0)
    content = (
      <GameOverScreen
        userNumber={userNumber}
        rounds={gameRounds}
        newGameHandler={newGameHandler}
      />
    );

  return (
    <View style={styles.container}>
      <Header title="Number Guessing" />
      {content}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
